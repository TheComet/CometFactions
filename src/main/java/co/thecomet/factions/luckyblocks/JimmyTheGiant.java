package co.thecomet.factions.luckyblocks;

import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

@LuckyInfo(name = "Jimmy The Giant", type = LuckyType.NORMAL, dropChance = 0.025)
public class JimmyTheGiant extends LuckyBlock {
    @Override
    public boolean canSpawn(Location loc, Player player) {
        if (loc.getWorld().getHighestBlockAt(loc).getY() == loc.getY()) {
            return true;
        }
        
        return false;
    }

    @Override
    public void onBreak(Block block, Player player) {
        Entity entity = block.getWorld().spawnEntity(block.getLocation().add(0.5, 1.0, 0.5), EntityType.GIANT);
        entity.setCustomName("Jimmy The Giant");
        entity.setCustomNameVisible(true);
    }
}
