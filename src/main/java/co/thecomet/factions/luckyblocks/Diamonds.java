package co.thecomet.factions.luckyblocks;

import co.thecomet.common.utils.GeneralUtils;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@LuckyInfo(name = "Diamonds", type = LuckyType.BOTH, dropChance = 0.025)
public class Diamonds extends LuckyBlock {
    @Override
    public void onBreak(Block block, Player player) {
        ItemStack diamonds = ItemBuilder.build(Material.DIAMOND).amount(GeneralUtils.getBetween(1, 3)).build();
        Location loc = block.getLocation();
        loc.getWorld().dropItem(loc.add(0.5, 1.0, 0.5), diamonds);
    }
}
