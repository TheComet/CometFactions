package co.thecomet.factions.luckyblocks;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@LuckyInfo(name = "Lucky Pickaxe", type = LuckyType.BOTH, dropChance = 0.025)
public class LuckyPickaxe extends LuckyBlock {
    private static ItemStack pickaxe;
    
    static {
        pickaxe = ItemBuilder.build(Material.GOLD_PICKAXE)
                .name(FontColor.rainbow("Lucky Pickaxe", false, false))
                .enchantment(Enchantment.DIG_SPEED, 4)
                .enchantment(Enchantment.DURABILITY, 3)
                .build();
    }
    
    @Override
    public void onBreak(Block block, Player player) {
        block.getWorld().dropItem(block.getLocation().add(0.5, 1.0, 0.5), pickaxe.clone());
    }
}
