package co.thecomet.factions.luckyblocks;

import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

@LuckyInfo(name = "Sumo Stick", type = LuckyType.BOTH, dropChance = 0.025)
public class SumoStick extends LuckyBlock implements Listener {
    private static ItemStack stick;
    
    static {
        stick = ItemBuilder.build(Material.BLAZE_ROD)
                .name("Sumo Stick")
                .enchantment(Enchantment.KNOCKBACK, 6)
                .build();
    }
    
    @Override
    public void onBreak(Block block, Player player) {
        Location loc = block.getLocation().add(0.5, 1.0, 0.5);
        loc.getWorld().dropItem(block.getLocation(), stick.clone());
    }
}
