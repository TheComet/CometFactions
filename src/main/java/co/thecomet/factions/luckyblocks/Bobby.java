package co.thecomet.factions.luckyblocks;

import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

@LuckyInfo(name = "Bobby", type = LuckyType.NORMAL, dropChance = 0.025)
public class Bobby extends LuckyBlock {
    @Override
    public void onBreak(Block block, Player player) {
        Location loc = block.getLocation();
        
        Entity entity = loc.getWorld().spawnEntity(loc, EntityType.ZOMBIE);
        entity.setCustomName("Bobby");
        entity.setCustomNameVisible(true);

        Zombie bobby = (Zombie) entity;
        bobby.getEquipment().setHelmet(ItemBuilder.build(Material.DIAMOND_HELMET).build());
        bobby.getEquipment().setChestplate(ItemBuilder.build(Material.DIAMOND_CHESTPLATE).build());
        bobby.getEquipment().setLeggings(ItemBuilder.build(Material.DIAMOND_LEGGINGS).build());
        bobby.getEquipment().setBoots(ItemBuilder.build(Material.DIAMOND_BOOTS).build());
        bobby.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0));
    }
}
