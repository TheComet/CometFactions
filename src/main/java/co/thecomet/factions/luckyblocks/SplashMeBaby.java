package co.thecomet.factions.luckyblocks;

import co.thecomet.common.utils.GeneralUtils;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

import java.util.ArrayList;
import java.util.List;

@LuckyInfo(name = "Splash Me Baby!", type = LuckyType.NORMAL, dropChance = 0.025)
public class SplashMeBaby extends LuckyBlock {
    private static List<PotionType> types = new ArrayList<>();
    
    static {
        types.add(PotionType.STRENGTH);
        types.add(PotionType.REGEN);
        types.add(PotionType.POISON);
    }
    
    @Override
    public void onBreak(Block block, Player player) {
        splash(player, types.get(GeneralUtils.getBetween(0, types.size())));
    }
    
    public static void splash(Player player, PotionType type) {
        // Create a potion type
        Potion potion = new Potion(type, 1);

        // Spawn the potion
        ThrownPotion thrown = (ThrownPotion) player.getWorld().spawnEntity(player.getLocation().add(0.0, 3.0, 0.0), EntityType.SPLASH_POTION);
        thrown.getEffects().addAll(potion.getEffects());
    }
}
