package co.thecomet.factions.luckyblocks;

import co.thecomet.core.CoreAPI;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

@LuckyInfo(name = "Entity Bomb", type = LuckyType.NORMAL)
public class EntityBomb extends LuckyBlock {
    @Override
    public void onBreak(Block block, Player player) {
        Location loc = block.getLocation().add(0.5, 1.0, 0.5);
        Entity entity = block.getWorld().spawnEntity(loc, EntityType.COW);

        Bukkit.getScheduler().runTaskLater(CoreAPI.getPlugin(), () -> {
            if (entity != null && entity.isDead() == false) {
                entity.getWorld().createExplosion(loc, 4.0f, true);
            }
        }, 20 * 2);
    }
}
