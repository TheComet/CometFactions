package co.thecomet.factions.luckyblocks;

import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.*;

@LuckyInfo(name = "Slime Invasion", type = LuckyType.NORMAL, dropChance = 0.025)
public class LuckySlimes extends LuckyBlock {
    @Override
    public void onBreak(Block block, Player player) {
        Location loc = block.getLocation().add(0.5, 1.0, 0.5);
        
        for (int i = 0; i < 5; i++) {
            Entity entity = loc.getWorld().spawnEntity(loc, EntityType.SLIME);
            Slime slime = (Slime) entity;
            slime.setSize(8);
            slime.setCustomName("Lucky Slime");
            slime.setCustomNameVisible(true);
        }
    }
}
