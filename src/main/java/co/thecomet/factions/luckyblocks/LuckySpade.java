package co.thecomet.factions.luckyblocks;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

@LuckyInfo(name = "Lucky Spade", type = LuckyType.BOTH, dropChance = 0.025)
public class LuckySpade extends LuckyBlock implements Listener {
    private static ItemStack spade;
    
    static {
        spade = ItemBuilder.build(Material.DIAMOND_SPADE)
                .enchantment(Enchantment.DIG_SPEED, 4)
                .enchantment(Enchantment.DURABILITY, 3)
                .name(FontColor.rainbow("Lucky Spade", false, false))
                .build();
    }
    
    @Override
    public void onBreak(Block block, Player player) {
        Location loc = block.getLocation();
        loc.getWorld().dropItem(block.getLocation().add(0.5, 1.0, 0.5), spade.clone());
    }
}
