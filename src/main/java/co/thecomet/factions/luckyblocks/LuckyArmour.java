package co.thecomet.factions.luckyblocks;

import co.thecomet.common.chat.FontColor;
import co.thecomet.common.utils.GeneralUtils;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

@LuckyInfo(name = "Lucky Armour", type = LuckyType.BOTH)
public class LuckyArmour extends LuckyBlock {
    private static List<Material> materials = new ArrayList<>();
    
    static {
        materials.add(Material.GOLD_HELMET);
        materials.add(Material.GOLD_CHESTPLATE);
        materials.add(Material.GOLD_LEGGINGS);
        materials.add(Material.GOLD_BOOTS);
    }
    
    @Override
    public void onBreak(Block block, Player player) {
        ItemStack armour = ItemBuilder.build(materials.get(GeneralUtils.getBetween(0, materials.size())))
                .name(FontColor.rainbow("Lucky Armour", false, false))
                .enchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2)
                .enchantment(Enchantment.DURABILITY, 2)
                .build();
        Location loc = block.getLocation().add(0.5, 1.0, 0.5);
        loc.getWorld().dropItem(loc, armour);
    }
}
