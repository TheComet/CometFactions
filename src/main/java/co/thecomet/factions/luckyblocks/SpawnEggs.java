package co.thecomet.factions.luckyblocks;

import co.thecomet.common.utils.GeneralUtils;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

@LuckyInfo(name = "Spawn Eggs", type = LuckyType.BOTH, dropChance = 0.025)
public class SpawnEggs extends LuckyBlock {
    private static List<EntityType> types = new ArrayList<>();
    
    static {
        types.add(EntityType.COW);
        types.add(EntityType.SHEEP);
        types.add(EntityType.CHICKEN);
        types.add(EntityType.HORSE);
    }
    
    @Override
    public void onBreak(Block block, Player player) {
        ItemStack eggs = ItemBuilder.build(Material.MONSTER_EGG)
                .durability(types.get(GeneralUtils.getBetween(0, types.size())).getTypeId())
                .amount(GeneralUtils.getBetween(1, 5))
                .build();
        Location loc = block.getLocation();
        loc.getWorld().dropItem(block.getLocation().add(0.5, 1.0, 0.5), eggs);
    }
}
