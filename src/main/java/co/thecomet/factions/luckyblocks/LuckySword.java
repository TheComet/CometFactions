package co.thecomet.factions.luckyblocks;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@LuckyInfo(name = "Lucky Sword", type = LuckyType.BOTH)
public class LuckySword extends LuckyBlock {
    private static Cache<UUID, Long> cooldown = CacheBuilder.newBuilder().expireAfterWrite(5, TimeUnit.SECONDS).build();
    private static ItemStack sword;
    
    static {
        sword = ItemBuilder.build(Material.GOLD_SWORD)
                .name(FontColor.rainbow("Lucky Sword", false, false))
                .enchantment(Enchantment.DAMAGE_ALL, 2)
                .enchantment(Enchantment.FIRE_ASPECT, 1)
                .enchantment(Enchantment.DURABILITY, 2)
                .build();
    }

    @Override
    public void onBreak(Block block, Player player) {
        Location loc = block.getLocation();
        loc.getWorld().dropItem(block.getLocation().add(0.5, 1.0, 0.5), sword.clone());
    }
}
