package co.thecomet.factions.luckyblocks;

import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

@LuckyInfo(name = "Rod of Destiny", type = LuckyType.BOTH, dropChance = 0.025)
public class RodOfDestiny extends LuckyBlock implements Listener {
    private static ItemStack rod;
    
    static {
        rod = ItemBuilder.build(Material.FISHING_ROD)
                .name("Rod of Destiny")
                .enchantment(Enchantment.DAMAGE_ALL, 3)
                .enchantment(Enchantment.KNOCKBACK, 3)
                .build();
    }
    
    @Override
    public void onBreak(Block block, Player player) {
        Location loc = block.getLocation().add(0.5, 1.0, 0.5);
        loc.getWorld().dropItem(block.getLocation(), rod.clone());
    }
}
