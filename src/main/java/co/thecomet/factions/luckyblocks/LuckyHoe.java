package co.thecomet.factions.luckyblocks;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.luckyblocks.LuckyBlock;
import co.thecomet.core.luckyblocks.LuckyInfo;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.utils.items.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

@LuckyInfo(name = "Lucky Hoe", type = LuckyType.BOTH)
public class LuckyHoe extends LuckyBlock implements Listener {
    private static ItemStack hoe;
    
    static {
        hoe = ItemBuilder.build(Material.DIAMOND_HOE)
                .name(FontColor.rainbow("Lucky Hoe", false, false))
                .enchantment(Enchantment.DIG_SPEED, 4)
                .enchantment(Enchantment.SILK_TOUCH, 1)
                .enchantment(Enchantment.DAMAGE_ARTHROPODS, 5)
                .enchantment(Enchantment.DURABILITY, 3)
                .build();
    }
    
    @Override
    public void onBreak(Block block, Player player) {
        block.getWorld().dropItem(block.getLocation().add(0.5, 1.0, 0.5), hoe.clone());
    }
}
