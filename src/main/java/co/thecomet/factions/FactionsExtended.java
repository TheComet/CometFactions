package co.thecomet.factions;

import co.thecomet.common.user.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.CorePlugin;
import co.thecomet.core.luckyblocks.LuckyBlocks;
import co.thecomet.core.luckyblocks.LuckyType;
import co.thecomet.core.luckyblocks.TriggerBlock;
import co.thecomet.core.moderation.commands.TeleportCommands;

import co.thecomet.factions.luckyblocks.*;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.Listener;

public class FactionsExtended extends CorePlugin implements Listener {
    private LuckyBlocks lb;
    
    @Override
    public void enable() { 
        this.lb = CoreAPI.getLuckyBlocks();
        lb.setCanCancel(true);
        lb.addTrigger(new TriggerBlock(Material.DIAMOND_ORE, LuckyType.VIP));
        
        Bukkit.getPluginManager().registerEvents(new TeleportCommands(true, Rank.FRIEND, true, Rank.FRIEND, true, Rank.DEFAULT, true, Rank.DEFAULT, true, Rank.ADMIN), this);
        registerLuckyBlocks();
    }
    
    private void registerLuckyBlocks() {
        lb.registerLuckyBlocks(AngryWolfPack.class, BabyZombieHorde.class, Bobby.class,
                DecoyChest.class, Diamonds.class, EntityBomb.class,
                Foodie.class, JimmyTheGiant.class, Lava.class,
                LookUp.class, LuckyArmour.class, LuckyAxe.class,
                LuckyChest.class, LuckyHoe.class, LuckyPickaxe.class,
                LuckySlimes.class, LuckySpade.class, LuckySword.class,
                RodOfDestiny.class, SpawnEggs.class, SplashMeBaby.class,
                SumoStick.class);
    }

    @Override
    public String getType() {
        return "FACTIONS";
    }

    @Override
    public String getStatus() {
        return getPlayersOnline() >= getMaxPlayers() ? "Full" : "Joinable";
    }
}
